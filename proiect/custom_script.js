$(document).ready(function(){  


    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }
     


      $(".send").click(function(){

        event.preventDefault(); 
        
        var modal_full_name = $(".modal_full_name").val();
        if(modal_full_name === ''){
            toastr["warning"]("Câmpul Full name este obligatoriu");
        }else{
            toastr["success"]("Formularul a fost completat cu succes");
        }



      });

});


